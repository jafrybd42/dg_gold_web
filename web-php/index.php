<?php

include_once  'credentials.php';

// header
include_once  'sub_view/header.php';

//login
if ($isLogin == 0) {
    include_once  'sub_view/index/login.php';
}

// body
include_once  'sub_view/index/investPart.php';
include_once  'sub_view/index/digitalGrow.php';
include_once  'sub_view/index/howItWorks.php';
include_once  'sub_view/index/partnerShip.php';
include_once  'sub_view/index/frequentQuestions.php';

// footer
include_once  'sub_view/footer.php';
