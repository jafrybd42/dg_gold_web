<?php

session_start();
if (!isset($_SESSION['dg_bangladesh_token'])) {
    header("location: ../index.php");
} else {
    include_once("../third_party_server/server.php");
    $server_obj = new Server();
    $reqData = array();
    $response = $server_obj->get_date("/product/list", $reqData, true);
    $product_list = array();

    if ($response['success'] == 1) {
        $product_list = $response['product_list'];
    }

?>


    <!DOCTYPE html>
    <html lang="en">
    <?php include_once('sub_view/head.php'); ?>

    <body>

        <?php include_once('sub_view/header.php'); ?>
        <!-- / header -->

        <?php include_once('sub_view/nav.php'); ?>
        <!-- / navigation -->

        <!-- / body -->

        <div id="body">
            <div class="container">
                <div class="pagination">
                    <ul>
                        <li><a href="#"><span class="ico-prev"></span></a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="ico-next"></span></a></li>
                    </ul>
                </div>
                <div class="products-wrap">
                    <!-- <aside id="sidebar">
                    <img src="images/ads.jpg" alt="Ads">
                </aside> -->
                    <div id="content">
                        <section class="products">
                            <?php for ($i = 0; $i < count($product_list); $i++) { ?>
                                <article>
                                    <img src="images/<?php echo  $product_list[$i]['image']; ?>" alt="">
                                    <h3> <?php echo $product_list[$i]['title']; ?></h3>
                                    <h4> <?php echo $product_list[$i]['karate_weight_gm']; ?> gm</h4>
                                    <a href="cart.html" class="btn-add">Add to cart</a>
                                </article>
                            <?php  } ?>
                        </section>
                    </div>
                    <!-- / content -->
                </div>
                <div class="pagination">
                    <ul>
                        <li><a href="#"><span class="ico-prev"></span></a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="ico-next"></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- / container -->
        </div>
        <!-- / body -->


        <?php include_once('sub_view/footer.php'); ?>
        <!-- / footer -->

        <?php include_once('sub_view/script.php'); ?>

    </body>

    </html>

<?php } ?>