<?php

include_once("req_html_responec.php");
include_once("../third_party_server/server.php");

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $NID = "dsa.com";
    $track_no = "dsa.com";
    $email = $_SESSION['dg_bangladesh_email'];

    if (isset($_POST['form_NID'])) {
        $NID = $_POST['form_NID'];
    }

    if (isset($_POST['form_track_no'])) {
        $track_no = $_POST['form_track_no'];
    }

    $req_data = [
        "NID" => $NID,
        "email" => $email,
        "track_no" => $track_no
    ];

    $server_obj = new Server();
    $responce = $server_obj->post_req("/auth/nid_check", $req_data, false);
    $next_step = "";
    $message = "";

    // print_r($responce);

    if ($responce != 0) {
        if ($responce['success']  == 1) {
            $next_step = $responce['next_step'];
            if ($next_step == 'home-page') { // login
                $user_name = $responce['user_info']['user_name'];
                $user_email = $responce['user_info']['user_email'];
                $token = $responce['token'];

                $_SESSION['dg_bangladesh_user_name'] =  $user_name;
                $_SESSION['dg_bangladesh_token'] = $token;
                $_SESSION['dg_bangladesh_user_email'] = $email;

                if (isset($_SESSION['dg_bangladesh_track_no'])) {
                    unset($_SESSION['dg_bangladesh_track_no']);
                }

                if (isset($_SESSION['dg_bangladesh_track_email'])) {
                    unset($_SESSION['dg_bangladesh_track_email']);
                }

                echo 0;
                return 0;
            }
        } else {
            $next_step = $responce['next_step'];
            if ($next_step == 'give_another_nid') {
                $next_step = 'NID-page';
                $message = $responce['message'];
            } else {
                $next_step = 'login-page';
                $message = $responce['message'];
                $track_no = 0;

                if (isset($_SESSION['dg_bangladesh_track_no'])) {
                    unset($_SESSION['dg_bangladesh_track_no']);
                }

                if (isset($_SESSION['dg_bangladesh_track_email'])) {
                    unset($_SESSION['dg_bangladesh_track_email']);
                }
            }
        }
    } else {
        $next_step = 'login-page';
        $message = 'Try again';
        $track_no = 0;

        if (isset($_SESSION['dg_bangladesh_track_no'])) {
            unset($_SESSION['dg_bangladesh_track_no']);
        }

        if (isset($_SESSION['dg_bangladesh_track_email'])) {
            unset($_SESSION['dg_bangladesh_track_email']);
        }
    }


    if ($next_step != "") {

        $html_responce_object = new HTML_Responce();
        $html_responce = $html_responce_object->get_html_by_next_step_name($next_step, $message, $track_no);
        echo $html_responce;
    }
}
