<?php
class HTML_Responce
{

    public $responce_html = array(
        "check-OTP-2" => '<div class="form-group" id="div_email_field">' .
            '<label for="from_OTP" class="col-form-label">OTP</label>' .
            '<input type="number" class="form-control" id="form_OTP" name="form_OTP" required>' .
            '</div>' .
            '<button type="button" onclick="checkOTP()" class="btn btn-primary">Submit OTP</button>',

        "check-OTP" => '<div class="lils382EmailInput">' .
            '<div class="group inf11Input"><input class="" style="font-size: 15px;" id="form_OTP" type="number" maxlength="6" min="6" max="999999" autocomplete="on" placeholder="" required=""><span class="bar"></span><label class="" style="font-size: 15px;">Give OTP</label></div>' .
            ' </div>' .
            ' <div class="lils382ContinueBtn">' .
            ' <div class="">' .
            ' <div class="btn51Btn btn51RipplePrimary btn51Primary" style="width: 100%; height: 50px;">' .
            '<div class="absolute-center btn51ParentDimension" onclick="checkOTP()"><span class="absolute-center" style="padding: 0px 25px;"><span>Continue</span></span></div>' .
            ' </div>' .
            ' </div>' .
            '</div>',

        "login-page-2" => '<div class="form-group" id="div_email_field">' .
            '<div class="form-group" id="div_email_field">' .
            '<label for="form_email" class="col-form-label">email</label>' .
            '<input type="email" class="form-control" id="form_email" name="form_email" required>' .
            '</div>' .
            '<button type="button" onclick="checkEmail()" class="btn btn-primary">Submit</button>',

        "login-page" =>  '<div class="lils382EmailInput">' .
            '<div class="group inf11Input"><input class="" style="font-size: 15px;" id="form_email" type="text" maxlength="250" min="0" max="10000000" autocomplete="on" placeholder="" required=""><span class="bar"></span><label class="" style="font-size: 15px;">Your Email Address or Phone No</label></div>' .
            ' </div>' .
            ' <div class="lils382ContinueBtn">' .
            ' <div class="">' .
            ' <div class="btn51Btn btn51RipplePrimary btn51Primary" style="width: 100%; height: 50px;">' .
            '<div class="absolute-center btn51ParentDimension" onclick="checkEmail()"><span class="absolute-center" style="padding: 0px 25px;"><span>Continue</span></span></div>' .
            ' </div>' .
            ' </div>' .
            '</div>',

        "NID-page-2"   => '<div class="form-group" id="div_email_field">' .
            '<label for="form_NID" class="col-form-label">NID --</label>' .
            '<input type="text" class="form-control" id="form_NID" name="form_NID" required>' .
            '</div>' .
            '<button type="button" onclick="checkNID()" class="btn btn-primary">Save changes</button>',

        "NID-page"   => '<div class="lils382EmailInput">' .
            '<div class="group inf11Input"><input class="" style="font-size: 15px;" id="form_NID" type="text" maxlength="250" min="0" max="10000000" autocomplete="on" placeholder="" required=""><span class="bar"></span><label class="" style="font-size: 15px;">Your NID Number</label></div>' .
            ' </div>' .
            ' <div class="lils382ContinueBtn">' .
            ' <div class="">' .
            ' <div class="btn51Btn btn51RipplePrimary btn51Primary" style="width: 100%; height: 50px;">' .
            '<div class="absolute-center btn51ParentDimension" onclick="checkNID()"><span class="absolute-center" style="padding: 0px 25px;"><span>Continue</span></span></div>' .
            ' </div>' .
            ' </div>' .
            '</div>',
    );

    function get_html_by_next_step_name($step_name, $message, $track_no)
    {
        // echo "message : " . $message;
        $res_html =  $this->responce_html[$step_name];

        if ($message != "") {
            $res_html = $res_html . '<br><br><p>' . $message . '</p>';
        }

        if ($track_no != 0 && $track_no != "") {
            $res_html = $res_html . '<input type="hidden" class="form-control" id="form_track_no" name="form_track_no" value="' . $track_no . '" required>';
        }
        return  $res_html;
    }
}
