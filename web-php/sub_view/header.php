<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="preload" as="script" href="js/website-assets/vendor/vendor.18b01aa4.js">
  <link rel="preload" href="js/website-assets/fonts/KFOlCnqEu92Fr1MmEU9fBBc4AMP6lQ.709f6f90.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="js/website-assets/fonts/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.ece6673e.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="stylesheet" href="./css/main.da8d5426.css">
  <link rel="stylesheet" href="./css/GoldLandingPage.69a4c447.css">
  <link rel="manifest" href="/manifest.json?v=1">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="application-name" content="Digital Gold Bangladesh">
  <meta name="apple-mobile-web-app-title" content="Digital Gold Bangladesh">
  <meta name="theme-color" content="#5500eb">
  <meta name="msapplication-navbutton-color" content="#5500eb">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="msapplication-starturl" content="/">
  <link type="application/opensearchdescription+xml" rel="search" href="/osdd.xml?v=3" />
  <link rel="icon" type="image/png" href="/favicon-32x32-groww.ico" sizes="32x32">
  <title data-react-helmet="true">Digital Gold: Buy 24K Gold Online, Invest in E Gold - Digital Gold Bangladesh</title>
  <link rel="stylesheet" type="text/css" href="css/own.css">
  <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  <link rel="stylesheet" href="css/demo.css"> <!-- Demo style -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
</head>

<body data-new-gr-c-s-check-loaded="8.872.0" data-gr-ext-installed="">

  <script>
    $('#leftMenu .panel-title a').click(function() {
      $('#leftMenu .panel-title a').parent('.panel-title').removeClass('accordion-opened');
      if ($(this).hasClass('opened')) {
        $(this).removeClass('opened');
        $(this).parent('.panel-title').removeClass('accordion-opened');
      } else {
        $(this).addClass('opened');
        $(this).parent('.panel-title').addClass('accordion-opened');
      }
    });
  </script>
  <div id="root">
    <div>
      <div id="goldLandingPage">
        <div id="goldLandingPageContent">
          <div id="header" style="min-height: 83px;">
            <div class="row mainDiv onMount-appear-done onMount-enter-done" style="border-bottom: medium none;">
              <div class="container web-align">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-sm-8">
                      <!-- <div class="row"> -->
                      <div class="row" style="min-height: 81px;">
                        <div class="col l5 absolute-center hdGrowwLogoDiv">
                          <div class="valign-wrapper cur-po pos-rel" style="min-width: 150px; min-height: 79px;" itemscope="true" itemtype="http://schema.org/Brand"><a href="index.php">
                              <img class="" src="./images/logo2.png" alt="Digital Gold Bangladesh Logo" itemprop="logo" width="408" height="100"></a></div>
                        </div>
                        <div class="col" style="margin-top: 50px;"><a class="hdQuickLabel" style="color:var(--text);margin-left: 50px;" href="explore.php">Explore</a></div>
                      </div>
                      <!-- </div> -->
                    </div>
                    <div class="col-sm-4">
                      <div class="col-sm-12">
                        <div class="col-sm-12">
                          <div class="col l12" style="min-height: 79px;">
                            <div class="col l12 valign-wrapper" style="min-height: 79px; justify-content: flex-end;">
                              <div style="min-width: 115px; min-height: 45px;">
                                <div>
                                  <div class="onMount-appear-done onMount-enter-done">
                                    <div class="">
                                      <div style="width: 175px; height: 45px; font-size: 16px;">
                                        <?php if ($isLogin == 1) { ?>
                                          <div class="absolute-center btn51ParentDimension cd-main-nav__list"><span class="absolute-center" style="padding: 0px 25px;">
                                              <span><a href="logout.php">Login Out</a></span>
                                            </span></div>
                                        <?php } else { ?>
                                          <div class="absolute-center btn51ParentDimension cd-main-nav__list js-signin-modal-trigger"><span class="absolute-center" style="padding: 0px 25px;">
                                              <span><a class="cd-main-nav__item cd-main-nav__item--signin" id="myBtn">Login/Register</a></span>
                                            </span></div>
                                        <?php } ?>

                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>