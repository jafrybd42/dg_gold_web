<div class="col l12">
    <div id="abtGrowwGold">
        <h3 class="componentsMainHeading" style="margin-top: 80px;">Digital Gold on Digital Gold Bangladesh</h3>
        <div class="abgg70Para">Digital Gold is a convenient and cost-effective way of purchasing gold online. Digital Gold Bangladesh offers Digital Gold of 99.90% purity that you can purchase, sell, and accumulate anytime at the live market rate, starting at Rs 10. Stored in 100% secured vaults and insured, you can view your purchased Digital Gold in your gold locker, which is a digital version for your holdings. <br><br>To provide you pure and top-quality bullion, Digital Gold Bangladesh has partnered with Augmont Goldtech Pvt. Ltd.— an integrated precious metals management company.</div>
        <div class="col l6 abgg70Cover">
            <div class="lazyload-wrapper"><img class="is31Default" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/buy_small_yellow.23af0db8.svg" alt="Digital Gold Bangladesh" width="40" height="40"></div>
            <div class="abgg70Heading">Buy small</div>
            <div class="abgg70SubHeading">Purchase gold in fractions for as low as 50 TK </div>
        </div>
        <div class="col l6 abgg70Cover">
            <div class="lazyload-wrapper"><img class="is31Default" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/sell_anytime_yellow.821df4c9.svg" alt="Digital Gold Bangladesh" width="40" height="40"></div>
            <div class="abgg70Heading">Sell anytime</div>
            <div class="abgg70SubHeading">Liquidate your Digital Gold and get your money in 2 days</div>
        </div>
        <div class="col l6 abgg70Cover">
            <div class="lazyload-wrapper"><img class="is31Default" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/safety_box_yellow.b534109d.svg" alt="Digital Gold Bangladesh" width="40" height="40"></div>
            <div class="abgg70Heading">100% secure</div>
            <div class="abgg70SubHeading">Stored in secured vaults and insured (verified by an independent trustee)</div>
        </div>
        <div class="col l6 abgg70Cover">
            <div class="lazyload-wrapper"><img class="is31Default" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/transparency_yellow.63c430e4.svg" alt="Digital Gold Bangladesh" width="40" height="40"></div>
            <div class="abgg70Heading">Transparent</div>
            <div class="abgg70SubHeading">Live market price tracking and zero making charges</div>
        </div>
        <div class="col l6 abgg70Cover">
            <div class="lazyload-wrapper"><img class="is31Default" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/diversify_yellow.56516643.svg" alt="Digital Gold Bangladesh" width="40" height="40"></div>
            <div class="abgg70Heading">Diversify</div>
            <div class="abgg70SubHeading">Balance your portfolio with Digital Gold to reduce risk concentration</div>
        </div>
        <div class="col l6 abgg70Cover">
            <div class="lazyload-wrapper"><img class="is31Default" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/exchange_yellow.48f9722e.svg" alt="Digital Gold Bangladesh" width="40" height="40"></div>
            <div class="abgg70Heading">Exchange</div>
            <div class="abgg70SubHeading">Convert your Digital Gold to physical gold in the form of coins/bars/jewellery (coming soon)</div>
        </div>
    </div>
</div>