<?php

$current_balance = 0;
if (isset($_SESSION['dg_bangladesh_token'])) {

    include_once("./third_party_server/server.php");
    $server_obj = new Server();
    $reqData = array();
    $response = $server_obj->post_req("/auth/balance", $reqData, true);

    $product_list = array();

    // print_r($response);
    if ($response['success'] == 1) {
        $current_balance = $response['data']['gold_22_karat'];
    }

?>

    <div class="outBox">
        <!-- <div id="abtGrowwGold">
            <h3 class="componentsMainHeading" style="margin-top: 80px;">Digital Gold Balance: <strong> <?php echo $current_balance; ?></strong> gm (22 Karate)</h3>
        </div> -->

        <div class="innerLeft">
            <h3 class="componentsMainHeading" style="margin-top: 7%;">Digital Gold Balance</h3>
        </div>
        <div class="innerRight">
            <h3><strong style='color: #05CA5E'> <?php echo $current_balance; ?></strong><br>
                <font color='#05CA5E'>gm (22 Karate)</font>
            </h3>
        </div>
    </div>

<?php } ?>