const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();

const product_model = require("../../model/product");
const common_model = require("../../model/common");
const cart_jewellery_model = require("../../model/cart_jewellery");
const gold_type_model = require("../../model/gold_type");
const balance_model = require("../../model/balance");
const buy_sell_trangaction_model = require("../../model/buy_sell_trangaction");

const verifyToken = require("../../jwt/verify/api_v1/verifyToken");

router.get('/list', verifyToken, async (req, res) => {
    let user_id = req.decoded.user_id;
    let product_list = await cart_jewellery_model.getCartJwelleryListByUserId(user_id);

    let gold_details = await gold_type_model.getGoldTypeById(1);
    let making_charge_percent = await common_model.jwelleryMakingCharge();
    let product_list_passing = [];

    let total_gold = 0;
    let making_charge = 0;

    // user Balance
    let user_balance_list = await balance_model.getUserBalanceByUserId(user_id);

    let userBalance = 0
    if (isEmpty(user_balance_list)) {
        await balance_model.addNewBalance({ "user_id": user_id });
    } else {
        userBalance = user_balance_list[0].gold_22_karat;
    }

    if (isEmpty(gold_details)) {
        return res.status(200).send({
            "success": true,
            "message": "",
            "current_balance": userBalance,
            "making_charge_percent": making_charge_percent,
            "total_gold_gm": total_gold,
            "making_charge": making_charge,
            "product_list": product_list_passing,
            "delivery_charge": 500
        });
    }


    let product_details = [];
    for (let index = 0; index < product_list.length; index++) {

        product_details = await product_model.getProductByID(product_list[index].product_id);
        product_list[index].product_details = isEmpty(product_details) ? {} : product_details[0];

        if (!isEmpty(product_details)) {
            product_list[index].product_details = product_details[0];
            if (product_list[index].hasOwnProperty('created_at')) delete product_list[index].created_at;

            total_gold += (product_details[0]['karate_weight_gm'] * product_list[index]['quantity']);
            product_list_passing.push(product_list[index]);
        }
    }

    try {
        let getGoldPriceAmount = await common_model.goldToTakaConvert(total_gold, gold_details[0].vori_price, 2);
        making_charge = (getGoldPriceAmount / 100) * making_charge_percent;
    } catch (error) {
        making_charge = 0;
    }

    return res.send({
        "success": true,
        "message": "",
        "current_balance": userBalance,
        "making_charge_percent": making_charge_percent,
        "total_gold_gm": total_gold,
        "making_charge": making_charge,
        "product_list": product_list_passing,
        "delivery_charge": 500
    });
});

router.post('/add', verifyToken, async (req, res) => {
    let product_id = req.body.product_id ?? 0;
    let user_id = req.decoded.user_id;
    let product_details = await product_model.getProductByID(product_id);

    if (isEmpty(product_details)) {
        return res.send({
            "success": false,
            "message": "Product Not found."
        });
    }

    let added_product_list = await cart_jewellery_model.getCartJwelleryByUser_IdAndProduct_Id(user_id, product_id);

    if (isEmpty(added_product_list)) {
        let new_cart_product = {
            "user_id": user_id,
            "product_id": product_id,
            "quantity": 1,
            "created_at": await common_model.getTodayDateTime
        }
        let add_product_to_cart_result = await cart_jewellery_model.addNewCartJwellery(new_cart_product);
    } else {
        let update_added_product_list_result = await cart_jewellery_model.updateCartJwelleryQuentityById(added_product_list[0].id, (added_product_list[0].quantity + 1));
    }

    return res.send({
        "success": true,
        "message": "Product Added to Cart.",
    });
});

router.post('/remove', verifyToken, async (req, res) => {
    let cart_id = req.body.cart_id ?? 0;
    let user_id = req.decoded.user_id;
    let card_details = await cart_jewellery_model.getCartJwelleryByUser_IdAndCart_Id(user_id, cart_id);

    if (isEmpty(card_details)) {
        return res.send({
            "success": false,
            "message": "Cart Not found."
        });
    }

    let cart_remove_result = await cart_jewellery_model.removeCartJwelleryById(cart_id);

    return res.send({
        "success": true,
        "message": "Product remove from Cart",
    });
});

router.post('/confirm_order', verifyToken, async (req, res) => {
    let user_id = req.decoded.user_id;

    let product_list = await cart_jewellery_model.getCartJwelleryListByUserId(user_id);
    let gold_details = await gold_type_model.getGoldTypeById(1);
    let making_charge_percent = await common_model.jwelleryMakingCharge();
    let product_list_passing = [];

    let total_gold = 0;
    let making_charge = 0;

    // user Balance
    let user_balance_list = await balance_model.getUserBalanceByUserId(user_id);

    let userBalance = 0
    if (isEmpty(user_balance_list)) {
        await balance_model.addNewBalance({ "user_id": user_id });
    } else {
        userBalance = user_balance_list[0].gold_22_karat;
    }


    if (isEmpty(gold_details)) {
        return res.status(200).send({
            "success": true,
            "message": "",
            "current_balance": userBalance,
            "making_charge_percent": making_charge_percent,
            "total_gold_gm": total_gold,
            "making_charge": making_charge,
            "product_list": product_list_passing,
            "delivery_charge": 500
        });
    }

    let product_details = [];
    for (let index = 0; index < product_list.length; index++) {

        product_details = await product_model.getProductByID(product_list[index].product_id);
        product_list[index].product_details = isEmpty(product_details) ? {} : product_details[0];

        if (!isEmpty(product_details)) {
            product_list[index].product_details = product_details[0];
            if (product_list[index].hasOwnProperty('created_at')) delete product_list[index].created_at;

            total_gold += (product_details[0]['karate_weight_gm'] * product_list[index]['quantity']);
            product_list_passing.push(product_list[index]);
        }
    }

    try {
        let getGoldPriceAmount = await common_model.goldToTakaConvert(total_gold, gold_details[0].vori_price, 2);
        making_charge = (getGoldPriceAmount / 100) * making_charge_percent;
    } catch (error) {
        making_charge = 0;
    }


    if (userBalance < total_gold) {
        return res.status(200).send({
            "success": true,
            "message": "Not available Balance.",
            "current_balance": userBalance,
            "making_charge_percent": making_charge_percent,
            "total_gold_gm": total_gold,
            "making_charge": making_charge,
            "product_list": product_list_passing,
            "delivery_charge": 500
        });
    }

    let todayTime = await common_model.getTodayDateTime;

    let jwellerY_buy_data = {
        "user_id": user_id,
        "totat_gold": total_gold,
        "making_charge": making_charge,
        "delivery_charge": 500,
        "receivable": Math.round(making_charge + 500),
        "order_details": JSON.stringify(product_list_passing),
        "created_at": todayTime
    }

    let buy_sell_transaction = {
        "user_id": user_id,
        "from_currency_type": "gold",
        "to_currency_type": "jwellery",
        "gold_type": 1,
        "gold_per_gram_price": Math.round(gold_details[0].vori_price / await common_model.getCurrentVoryWightInGram()),
        "sub_total_money": 0,
        "vat_money": 0,
        "making_cost": making_charge,
        "service_charge": 500,
        "total_money": Math.round(making_charge + 500),
        "total_gold": total_gold,
        "created_at": todayTime
    }

    let userNewBalance = userBalance - total_gold;

    let trangaction = await buy_sell_trangaction_model.addNewBuy_sellAndAddBuy_JwelleryRemoveAllCardAndUserGoldBalance(jwellerY_buy_data, buy_sell_transaction, userNewBalance, "gold_22_karat", user_id );
    return res.send({
        "success": true,
        "message": "All Buy Successfully Done",
        "current_balance": userNewBalance,
        "making_charge_percent": making_charge_percent,
        "total_gold_gm": 0,
        "making_charge": 0,
        "product_list": [],
        "delivery_charge": 0
    });

});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



module.exports = router;