const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();

const gold_type_model = require("../../model/gold_type");


router.get('/price', async (req, res) => {
    let karat_type = req.query.karat_type ?? 0;
    let gold_type_details = await gold_type_model.getGoldTypeById(karat_type);
    let vori_price = 0;

    if(!isEmpty(gold_type_details)){
        vori_price = gold_type_details[0].vori_price
    }

    return res.send({
        "success": true,
        "message": "",
        "price": vori_price
    });
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



module.exports = router;