const mysql = require('mysql');

const connectionDGBD = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "", 
    database: "digital_gold_bd"
});

// var connectionDGBD  = mysql.createPool({
//     connectionLimit : 100,
//     host: "localhost",
//     user: "root",
//     password: "", 
//     database: "digital_gold_bd"
//   });


module.exports = {
    connectionDGBD
}