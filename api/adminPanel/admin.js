const express = require("express");
const router = express.Router();

const isEmpty = require("is-empty");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var path = require('path');
const multer = require('multer');

const admin_model = require('../model/admin');
const common_model = require('../model/common');
const verifyTokenWeb = require('../jwt/verify/verifyTokenWeb');

const user_route = require('./user');
const gold_route = require('./gold');

const url = "http://localhost:3001/admin_panel";

router.use("/user", user_route);
router.use("/gold", gold_route);

router.get(['/home', '/'], (req, res) => {
    return res.render('home', {
        message: "",
        page: "home",
        success: true
    });
});


router.get("/login", (req, res) => {
    if (req.session.is_login_admin === true) {
        return res.redirect(url + "/home");
    }
    return res.render('login', {
        page: "login",
        message: "",
        "success": false
    });
});


router.post("/login", async (req, res) => {
    let loginData = {
        email: req.body.email,
        password: req.body.password
    }

    let successMessage = "";
    let issuccess = 0;

    if (isEmpty(loginData.email) || loginData.email == undefined) {
        issuccess = 1;
        successMessage += "Email is invalid. ";
    } else {
        const re = /\S+@\S+\.\S+/;
        if (!re.test(loginData.email)) {
            issuccess = 1;
            successMessage += "Email is invalid. ";
        }
    }

    if (isEmpty(loginData.password) || loginData.password == undefined || loginData.password.length < 6) {
        issuccess = 1;
        successMessage += "Need valid password. ";
    }

    if (issuccess == 1) {
        return res.render(url + 'login', {
            "page": "Home",
            "success": true,
            "message": successMessage
        });
    }

    const adminInfo = await admin_model.getAdminByEmail(loginData.email);

    if (isEmpty(adminInfo)) {
        return res.render(url + 'login', {
            "page": "Home",
            "success": true,
            "message": "No user found."
        });
    } else if (adminInfo[0].status != 1) {
        return res.render(url + 'login', {
            "page": "Home",
            "success": true,
            "message": "Sorry Your account is delete."
        });
    }

    // return res.send(adminInfo[0])

    if (bcrypt.compareSync(loginData.password, adminInfo[0].password)) {
        let TokenData = {
            "id": adminInfo[0].id,
            "organization_id": adminInfo[0].organization_id,
            "email": adminInfo[0].email,
            "name": adminInfo[0].name
        }
        console.trace("come");
        if (adminInfo[0].hasOwnProperty("password")) delete adminInfo[0].password;
        if (adminInfo[0].hasOwnProperty("status")) delete adminInfo[0].status;
        if (adminInfo[0].hasOwnProperty("created_at")) delete adminInfo[0].created_at;
        if (adminInfo[0].hasOwnProperty("updated_at")) delete adminInfo[0].updated_at;

        // JSON.parse(JSON.stringify(adminInfo[0]))
        let token = jwt.sign(TokenData, global.config.secretKey, {
            algorithm: global.config.algorithm,
            expiresIn: '300m' //
        });
        console.trace("come");
        req.session.bh_token = token;
        req.session.is_login_admin = true;

        return res.redirect(url + "/home")
    } else {
        console.trace("come");
        // return res.send({
        //     "admin": adminInfo[0],
        //     "url": url + '/login'
        // })
        return res.render('/login', {
            "page": "Home",
            "success": true,
            "message": "Wrong Password"
        });
    }
});


router.get('/logout', verifyTokenWeb, (req, res) => {
    req.session.destroy(() => {
        console.log("Session distroyed");
    });
    res.redirect('/login');
});


// router.get(['/user-list'], (req, res) => {
//     return res.render('user_list', {
//         message: "",
//         page: "user-list",
//         success: true
//     });
// });


// router.get(['/entry'], (req, res) => { //verifyTokenWeb
//     return res.render('dataEntry', {
//         message: "",
//         page: "entry"
//     });
// });



// router.post('/AddMedicalInfo', (req, res) => {
//     return res.send({
//         "data": req.body
//     })
// });

router.get('/send_email', async (req, res) => {
    let result = await common_model.sendEmail("tareqmahin35@gmail.com");
    return res.send({
        "data": result
    })
});

router.get('/hash', async (req, res) => {
    let result = await common_model.hashAString();
    return res.send({
        "data": result
    });
});


router.get('/*', (req, res) => {
    return res.render('404', {
        message: "Page Not Found",
        page: "list"
    });
});



router.post('/*', (req, res) => {
    return res.render('404', {
        message: "Page Not Found",
        page: "list"
    });
});



module.exports = router;